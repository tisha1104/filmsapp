﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using FilmsApp.Services.Films;
using FilmsApp.Services.Films.Helpers;
using FilmsApp.Services.Blobs;
using FilmsApp.Data;
using FilmsApp.Controllers;
using FilmsApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace FilmsApp.Tests.Services.Films
{
    public class FilmsServiceTests
    {
        [Fact]
        public void ShowUserFilmRate_UserDoesntHaveFilmRate_ReturnsMinus1()
        {
            var options = new DbContextOptionsBuilder<FilmsDbContext>()
                               .UseInMemoryDatabase(databaseName: "FilmsTest")
                               .Options;

            using (var context = new FilmsDbContext(options))
            {
                var mockHelper = new Mock<IFilmsHelper>();
                var mockBlob = new Mock<IBlobService>();
                var mockEnv = new Mock<IHostingEnvironment>();
                var service = new FilmsService(context, mockHelper.Object, mockBlob.Object, mockEnv.Object);
                var rateModel = new RateModel()
                {
                    Username = "test",
                    FilmId = 1
                };

                var result = service.GetUserFilmRate(rateModel);

                Assert.Equal(-1, result);
            }
        }
    }
}
