﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using FilmsApp.Services.Films;
using FilmsApp.Services.Films.Helpers;
using FilmsApp.Services.Blobs;
using FilmsApp.Data;
using FilmsApp.Controllers;
using FilmsApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using FilmsApp.Services.Users;
using FilmsApp.Data.DatabaseModels;

namespace FilmsApp.Tests.Services.Users
{
    public class UsersServiceTests
    {
        [Fact]
        public void Login_TestUsername_UsernameEqual()
        {
            var options = new DbContextOptionsBuilder<FilmsDbContext>()
                               .UseInMemoryDatabase(databaseName: "FilmsTest")
                               .Options;

            using (var context = new FilmsDbContext(options))
            {
                var testString = "test10111";
                var user = new User()
                {
                    Username = testString,
                    BirthDate = DateTime.Now,
                    ConnectionLink = testString,
                    FatherName = testString,
                    FirstName = testString,
                    LastName = testString,
                    Mail = testString,
                    Sex = "м",
                    Role = "1"
                };
                context.User.Add(user);
                context.SaveChanges();
                var userPassword = new UserPassword()
                {
                    Username = testString,
                    Password = testString
                };
                context.UserPassword.Add(userPassword);
                context.SaveChanges();
                var loginModel = new LoginModel()
                {
                    Username = testString,
                    Password = testString
                };
                var service = new UserService(context);

                var result = service.Login(loginModel);

                Assert.Equal(loginModel.Username, result.Username);
            }
        }

        [Fact]
        public void Login_TestWrongPassword_ReturnNull()
        {
            var options = new DbContextOptionsBuilder<FilmsDbContext>()
                               .UseInMemoryDatabase(databaseName: "FilmsTest")
                               .Options;

            using (var context = new FilmsDbContext(options))
            {
                var testString = "test101113";
                var user = new User()
                {
                    Username = testString,
                    BirthDate = DateTime.Now,
                    ConnectionLink = testString,
                    FatherName = testString,
                    FirstName = testString,
                    LastName = testString,
                    Mail = testString,
                    Sex = "м",
                    Role = "1"
                };
                context.User.Add(user);
                context.SaveChanges();
                var userPassword = new UserPassword()
                {
                    Username = testString,
                    Password = testString
                };
                context.UserPassword.Add(userPassword);
                context.SaveChanges();
                var loginModel = new LoginModel()
                {
                    Username = testString,
                    Password = "test123pass"
                };
                var service = new UserService(context);

                var result = service.Login(loginModel);

                Assert.Null(result);
            }
        }

        [Fact]
        public void SignUp_TestUserCreated_ReturnUserModel()
        {
            var options = new DbContextOptionsBuilder<FilmsDbContext>()
                               .UseInMemoryDatabase(databaseName: "FilmsTest")
                               .Options;

            using (var context = new FilmsDbContext(options))
            {
                var testString = "test101117";
                var signUpModel = new SignUpModel()
                {
                    Username = testString,
                    BirthDate = DateTime.Now,
                    ConnectionLink = testString,
                    FatherName = testString,
                    FirstName = testString,
                    LastName = testString,
                    Mail = testString,
                    Sex = "м",
                    Password = testString
                };
                var service = new UserService(context);

                var userModel = service.SignUp(signUpModel);

                var result = context.User.Find(signUpModel.Username);
                
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void SignUp_TestPasswordCreated_ReturnUserModel()
        {
            var options = new DbContextOptionsBuilder<FilmsDbContext>()
                               .UseInMemoryDatabase(databaseName: "FilmsTest")
                               .Options;

            using (var context = new FilmsDbContext(options))
            {
                var testString = "test101118";
                var signUpModel = new SignUpModel()
                {
                    Username = testString,
                    BirthDate = DateTime.Now,
                    ConnectionLink = testString,
                    FatherName = testString,
                    FirstName = testString,
                    LastName = testString,
                    Mail = testString,
                    Sex = "м",
                    Password = testString
                };
                var service = new UserService(context);

                var userModel = service.SignUp(signUpModel);

                var result = context.UserPassword.Find(signUpModel.Username);

                Assert.NotNull(result);
            }
        }
    }
}
