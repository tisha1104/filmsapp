﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using FilmsApp.Services.Films;
using FilmsApp.Services.Films.Helpers;
using FilmsApp.Services.Blobs;
using FilmsApp.Data;
using Microsoft.Extensions.Hosting;
using FilmsApp.Controllers;
using FilmsApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace FilmsApp.Tests.Controllers
{
    public class FilmsControllerTests
    {
        [Fact]
        public void SearchFilm_FindFilmIncludeLetterMTest_ReturnsListWithFilms()
        {
            var mock = new Mock<IFilmsService>();
            var controller = new FilmsController(mock.Object);
            var searchModel = new SearchModel()
            {
                FilmName = "м"
            };
            
            var value = (controller.SearchFilm(searchModel) as JsonResult)?.Value;
            var result = value as IEnumerable<FilmModel>;
            
            foreach (var film in result)
            {
                bool contains = film.Name.Contains("м") ? true : false;
                Assert.True(contains);
            }
        }

    }
}
