using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class CommentRequestModel
    {
        public long FilmId { get; set; }
        public bool? IsDateASC { get; set;}
        public int? Page { get; set; }
        public long? ParentCommentId { get; set; }
    }
}
