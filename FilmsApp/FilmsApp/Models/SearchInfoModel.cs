using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class SearchInfoModel
    {
        public Dictionary<long, string> Categories { get; set; }
        public Dictionary<long, string> Countries { get; set; }
        public Dictionary<long, string> StageManagers { get; set; }
    }
}
