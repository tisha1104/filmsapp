using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class CommentModel
    {
        public long? CommentId { get; set; }
        public string Username { get; set; }
        public long FilmId { get; set; }
        public long? ParentCommentId { get; set; }
        public string Date { get; set; }
        public string CommentText { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
