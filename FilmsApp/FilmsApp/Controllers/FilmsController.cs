using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsApp.Models;
using FilmsApp.Services.Films;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FilmsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmsController : Controller
    {
        private IFilmsService filmsService;

        public FilmsController(IFilmsService filmsService)
        {
            this.filmsService = filmsService;
        }

        [HttpGet("accepted")]
        public IActionResult ShowAccepted()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetAccepted());

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpGet("new")]
        public IActionResult ShowNew()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetNew());

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpGet("top")]
        public IActionResult ShowTop()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetTop());

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpGet("random")]
        public IActionResult ShowRandom()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetRandom());
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("search")]
        public IActionResult SearchFilm(SearchModel searchModel)
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.SearchFilm(searchModel));

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpGet("getSearchInfo")]
        public IActionResult GetSearchInfo()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetSearchInfo());

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("addFilm")]
        [DisableRequestSizeLimit]
        public IActionResult AddFilm([FromForm]FilmModel filmModel)
        {
            JsonResult result;
            try
            {
                var filmAdded = filmsService.AddFilm(filmModel);
                if (filmAdded)
                {
                    result = Json(Ok("Film was added"));
                }
                else
                {
                    result = Json(BadRequest("Film wasn't added"));
                }
                result = Json(Ok("Film was added"));
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }


        [HttpPost("userFilms")]
        public IActionResult GetUserFilms(UserModel userModel)
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetUserFilms(userModel.Username));

            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpGet("suggestions")]
        public IActionResult GetSuggestions()
        {
            JsonResult result;
            try
            {
                result = Json(filmsService.GetFilmsSuggestions());
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("publishFilm")]
        public IActionResult AcceptFilm(FilmModel filmModel)
        {
            JsonResult result;
            try
            {
                var acceptMade = filmsService.AcceptFilm(filmModel.FilmId);
                if (acceptMade)
                {
                    result = Json(Ok());
                }
                else
                {
                    result = Json(BadRequest("Операция не выполнена"));
                }
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("userFilmRateGet")]
        public IActionResult ShowUserFilmRate(RateModel rateModel)
        {
            JsonResult result;
            try
            {
                var rate = filmsService.GetUserFilmRate(rateModel);
                result = Json(Ok(rate));
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("userFilmRatePost")]
        public IActionResult SetUserFilmRate(RateModel rateModel)
        {
            JsonResult result;
            try
            {
                var rateNew = filmsService.SetUserFilmRate(rateModel);
                result = Json(Ok(rateNew));
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("deleteFilm")]
        public IActionResult DeleteFilm(FilmModel filmModel)
        {
            JsonResult result;
            try
            {
                var deleteComplite = filmsService.DeleteFilm(filmModel.FilmId);
                if(deleteComplite)
                {
                    result = Json(Ok("Film was deleted"));
                }
                else
                {
                    result = Json(BadRequest("Film was not deleted"));
                }
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("denyFilm")]
        public IActionResult DenyFilm(FilmModel filmModel)
        {
            JsonResult result;
            try
            {
                var deleteComplite = filmsService.DenyFilm(filmModel.FilmId);
                if (deleteComplite)
                {
                    result = Json(Ok("Film was denied"));
                }
                else
                {
                    result = Json(BadRequest("Film was not denied"));
                }
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("filmBlobVideo")]
        public IActionResult FindFilmVideoBlob(FilmModel filmModel)
        {
            JsonResult result;
            try
            {
                var path = filmsService.FindFilmVideoBlob(filmModel.VideoFile);
                if (path != null)
                {
                    result = Json(Ok(path));
                }
                else
                {
                    result = Json(BadRequest("Video not found"));
                }
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

    }
}
