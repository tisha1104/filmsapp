using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FilmsApp.Controllers
{
  /// <summary>
  /// HomeController provides opening of the app
  /// </summary>
  public class HomeController : Controller
  {
      /// <summary>
      /// Method opens main view
      /// </summary>
      /// <returns>Should return view</returns>
      public IActionResult Index()
      {
          return View();
      }
  }
}
