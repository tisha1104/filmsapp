using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsApp.Models;
using Microsoft.AspNetCore.Mvc;
using FilmsApp.Services.Comments;

namespace FilmsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : Controller
    {
        private ICommentsService commentsService;

        public CommentController(ICommentsService commentsService)
        {
            this.commentsService = commentsService;
        }

        [HttpPost("add")]
        public IActionResult AddComment([FromBody]CommentModel commentModel)
        {
            IActionResult result;
            try
            {
                commentsService.AddComment(commentModel);
                result = Json("Added");
            }
            catch (Exception e)
            {
                result = BadRequest(e.Message);
            }

            return result;
        }

        [HttpPost("delete")]
        public IActionResult DeleteComment([FromBody]CommentModel commentModel)
        {
            IActionResult result;
            try
            {
                commentsService.DeleteComment(commentModel);
                result = Json("Deleted");
            }
            catch (Exception e)
            {
                result = BadRequest(e.Message);
            }

            return result;
        }

        [HttpPost("comments")]
        public IActionResult GetComments([FromBody]CommentRequestModel requestModel)
        {
            IActionResult result;
            try
            {
                result = Json(commentsService.GetComments(requestModel));
            }
            catch (Exception e)
            {
                result = BadRequest(e.Message);
            }
            return result;
        }

        [HttpPost("parent")]
        public IActionResult GetParentComment([FromBody]CommentRequestModel requestModel)
        {
            IActionResult result;
            try
            {
                result = Json(commentsService.GetParentComment(requestModel));
            }
            catch (Exception e)
            {
                result = BadRequest(e.Message);
            }
            return result;
        }
    }
}
