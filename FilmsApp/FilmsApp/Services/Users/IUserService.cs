using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Users
{
    public interface IUserService
    {
        UserModel Login(LoginModel loginModel);
        UserModel SignUp(SignUpModel signUpModel);
    }
}
