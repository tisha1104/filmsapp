using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace FilmsApp.Services.Blobs
{
    public class BlobService: IBlobService
    {

        private IHostingEnvironment appEnvironment;

        public BlobService(IHostingEnvironment appEnvironment)
        {
            this.appEnvironment = appEnvironment;
        }
        //public ActionResult CreateBlobContainerFoto()
        //{
        //    CloudBlobContainer container = GetCloudBlobContainerPhoto();
        //    container.CreateIfNotExistsAsync();
        //    var name = container.Name;
        //    return Json(name);
        //}

        //public ActionResult CreateBlobContainerVideo()
        //{
        //    CloudBlobContainer container = GetCloudBlobContainerVideo();
        //    container.CreateIfNotExistsAsync();
        //    var name = container.Name;
        //    return Json(name);
        //}

        public void UploadBlobPhoto(string reference, IFormFile file)
        {
            var pathPart = @"\img\" + reference + ".png";
            var path = appEnvironment.WebRootPath + pathPart;

            var stream = new FileStream(path, FileMode.Create, System.IO.FileAccess.Write);
            using (var fileStream = stream)
            {
                file.CopyToAsync(fileStream);
            }

            //CloudBlobContainer container = GetCloudBlobContainerPhoto();
            //CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            //using (var fileStream = file.OpenReadStream())
            //{
            //    blob.UploadFromStreamAsync(fileStream).Wait();
            //}
            return;
        }

        public void UploadBlobVideo(string reference, IFormFile file)
        {
            CloudBlobContainer container = GetCloudBlobContainerVideo();
            CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            using (var fileStream = file.OpenReadStream())
            {
                blob.UploadFromStreamAsync(fileStream).Wait();
            }
            return;
        }
        
        public Stream DownloadBlobPhoto(string reference)
        {
            CloudBlobContainer container = GetCloudBlobContainerPhoto();
            CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            var stream = new MemoryStream();
            using (var fileStream = stream)
            {
                blob.DownloadToStreamAsync(fileStream).Wait();
                stream.Position = 0;
                return stream;
            }
        }

        public string DownloadBlobVideo(string reference)
        {
            var pathPart = @"\video\" + reference + ".mp4";
            var path = appEnvironment.WebRootPath + pathPart;

            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                return pathPart;
            }

            CloudBlobContainer container = GetCloudBlobContainerVideo();
            CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            var stream = new FileStream(path, FileMode.Create, System.IO.FileAccess.Write);
            using (var fileStream = stream)
            {
                blob.DownloadToStreamAsync(fileStream).Wait();
                stream.Position = 0;
                return pathPart;
            }
        }

        public void DeleteBlobPhoto(string reference)
        {
            CloudBlobContainer container = GetCloudBlobContainerPhoto();
            CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            blob.DeleteAsync().Wait();
            return;
        }

        public void DeleteBlobVideo(string reference)
        {
            CloudBlobContainer container = GetCloudBlobContainerVideo();
            CloudBlockBlob blob = container.GetBlockBlobReference(reference);
            blob.DeleteAsync().Wait();
            return;
        }
        
        private CloudBlobContainer GetCloudBlobContainerVideo()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            IConfigurationRoot Configuration = builder.Build();
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(Configuration["ConnectionStrings:AzureStorageConnectionString-1"]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("filmsappvideo");
            return container;
        }

        private CloudBlobContainer GetCloudBlobContainerPhoto()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            IConfigurationRoot Configuration = builder.Build();
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(Configuration["ConnectionStrings:AzureStorageConnectionString-1"]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("filmsappphoto");
            return container;
        }
    }
}
