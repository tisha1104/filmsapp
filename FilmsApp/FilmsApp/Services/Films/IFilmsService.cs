using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Films
{
    public interface IFilmsService
    {
        FilmModel BuildModel(long filmId);
        IEnumerable<FilmModel> GetAccepted();
        IEnumerable<FilmModel> SearchFilm(SearchModel searchModel);
        IEnumerable<FilmModel> GetTop();
        IEnumerable<FilmModel> GetNew();
        IEnumerable<FilmModel> GetRandom();
        SearchInfoModel GetSearchInfo();
        bool AddFilm(FilmModel filmModel);
        IEnumerable<FilmModel> GetUserFilms(string username);
        IEnumerable<FilmModel> GetFilmsSuggestions();
        bool AcceptFilm(long filmId);
        double GetUserFilmRate(RateModel rateModel);
        double SetUserFilmRate(RateModel rateModel);
        bool DeleteFilm(long filmId);
        bool DenyFilm(long filmId);
        string FindFilmVideoBlob(string filmReference);

    }
}
