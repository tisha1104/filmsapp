using FilmsApp.Data;
using FilmsApp.Data.DatabaseModels;
using FilmsApp.Models;
using FilmsApp.Services.Blobs;
using FilmsApp.Services.Films.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace FilmsApp.Services.Films
{
    public class FilmsService: IFilmsService
    {
        private FilmsDbContext dbContext;
        private IFilmsHelper filmsHepler;
        private IBlobService blobService;
        private IHostingEnvironment appEnvironment;

        public FilmsService(FilmsDbContext dbContext, IFilmsHelper filmsHepler, IBlobService blobService, IHostingEnvironment appEnvironment)
        {
            this.dbContext = dbContext;
            this.filmsHepler = filmsHepler;
            this.blobService = blobService;
            this.appEnvironment = appEnvironment;
        }

        public IEnumerable<FilmModel> GetAccepted()
        {
            var filmsAcc = GetAcceptedFilms();
            IEnumerable<FilmModel> filmModels = BuildModel(filmsAcc);
            return filmModels;
        }

        public IEnumerable<FilmModel> GetTop()
        {
            var filmsAcc = GetAcceptedFilms();
            var films = dbContext.Film.Where(o => filmsAcc.Contains(o.Id)).OrderByDescending(o => o.Rate).Take(10);
            var filmsId = new List<long>();
            foreach(var film in films)
            {
                filmsId.Add(film.Id);
            }
            IEnumerable<FilmModel> filmModels = BuildModel(filmsId);
            return filmModels;
        }
        
        public IEnumerable<FilmModel> GetNew()
        {
            var filmsAcc = GetAcceptedFilms();
            var films = dbContext.Film.Where(o => filmsAcc.Contains(o.Id)).OrderByDescending(o => o.Date).Take(10);
            var filmsId = new List<long>();
            foreach (var film in films)
            {
                filmsId.Add(film.Id);
            }
            IEnumerable<FilmModel> filmModels = BuildModel(filmsId);
            return filmModels;
        }

        public IEnumerable<FilmModel> GetRandom()
        {
          var films = GetAcceptedFilms().OrderBy(o => Guid.NewGuid()).Take(10).ToList();
          IEnumerable<FilmModel> filmModels = BuildModel(films);
          return filmModels;
        }

        public IEnumerable<FilmModel> SearchFilm(SearchModel searchModel)
        {
            var films = DbSearchFilm(searchModel);
            IEnumerable<FilmModel> filmModels = BuildModel(films);
            return filmModels;
        }

        public FilmModel BuildModel(long filmId)
        {
            var film = filmsHepler.FindFilm(filmId);
            //var fileForm = File.Create(blobService.DownloadBlobVideo(film.VideoFile),"video/mp4", film.Name);

            string photoReference = "/img/no-image.jpg";

            var pathPart = @"\img\" + Convert.ToString(film.Id) + ".png";
            var path = appEnvironment.WebRootPath + pathPart;

            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                photoReference = pathPart;
            }

            var filmModel = new FilmModel()
            {
                Name = film.Name,
                ProductionYear = film.ProductionYear,
                NameCameraMan = filmsHepler.GetFilmParticipentNameAndSurname(film.CameraMan),
                NameEditedBy = filmsHepler.GetFilmParticipentNameAndSurname(film.EditedBy),
                NameMusicBy = filmsHepler.GetFilmParticipentNameAndSurname(film.MusicBy),
                NameProducer = filmsHepler.GetFilmParticipentNameAndSurname(film.Producer),
                NameScriptWriter = filmsHepler.GetFilmParticipentNameAndSurname(film.ScriptWriter),
                NameStageManager = filmsHepler.GetFilmParticipentNameAndSurname(film.StageManager),
                AgePermition = film.AgePermition,
                Rate = film.Rate,
                Date = film.Date.ToString("Y"),
                MinuteDuration = film.MinuteDuration,
                FilmId = filmId,
                VideoFile = film.VideoFile,
                PhotoFile = photoReference,
                //VideoFileForm = (IFormFile)(blobService.DownloadBlobVideo(film.VideoFile)),
                AuthorLink = film.AuthorLink,
                Categories = filmsHepler.FindCategories(filmId),
                Actors = filmsHepler.FindActors(filmId),
                Countries = filmsHepler.FindCountries(filmId),
                Status= GetFilmStatus(filmId)
            };
            return filmModel;
        }

        public SearchInfoModel GetSearchInfo()
        {
            var countries = dbContext.Country.ToList();
            var countriesDictionary = new Dictionary<long, string>();
            foreach (var country in countries)
            {
              countriesDictionary[country.Id] = country.Name;
            }

            var categories = dbContext.Category.ToList();
            var categoriesDictionary = new Dictionary<long, string>();
            foreach (var category in categories)
            {
                categoriesDictionary[category.Id] = category.Name;
            }

            var films = dbContext.Film.ToList();
            var stageManagersDictionary = new Dictionary<long, string>();
            foreach(var film in films)
            {
                stageManagersDictionary[film.StageManager] = filmsHepler.GetFilmParticipentNameAndSurname(film.StageManager);
            }

            var searchInfoModel = new SearchInfoModel()
            {
              Categories = categoriesDictionary,
              Countries = countriesDictionary,
              StageManagers = stageManagersDictionary
            };

            return searchInfoModel;
        }

        public IEnumerable<FilmModel> GetUserFilms(string username)
        {
            var clientFilmsAdded = dbContext.ClientFilmAdded.Where(o => o.Username == username);
            var filmModels = new List<FilmModel>();
            foreach(var clientFilm in clientFilmsAdded)
            {
                filmModels.Add(BuildModel(clientFilm.FilmId));
            }
            return filmModels;
        }

        public IEnumerable<FilmModel> GetFilmsSuggestions()
        {
            var filmModels = new List<FilmModel>();
            var filmsSuggested = dbContext.ClientFilmAdded.Where(o => o.Status == "0").ToList();
            foreach (var film in filmsSuggested)
            {
                filmModels.Add(BuildModel(film.FilmId));
            }
            return filmModels;
        }

        public bool AcceptFilm(long filmId)
        {
            bool result = false;
            var accepted = dbContext.ClientFilmAdded.Where(o => o.FilmId == filmId).ToList()[0];
            if(accepted !=null)
            {
                accepted.Status = "1";
                dbContext.SaveChanges();
                result = true;
            }

            return result;
        }

        public double GetUserFilmRate(RateModel rateModel)
        {
            var userFilmRate = dbContext.ClientFilmRate.Find(rateModel.FilmId, rateModel.Username);
            double rate = -1;
            if(userFilmRate == null)
            {
                rate = -1;
            }
            else
            {
                rate = userFilmRate.Rate;
            }
            return rate;
        }

        public double SetUserFilmRate(RateModel rateModel)
        {
            var clientFilmRate = new ClientFilmRate()
            {
                FilmId = rateModel.FilmId,
                Username = rateModel.Username,
                Rate = rateModel.Rate
            };

            dbContext.ClientFilmRate.Add(clientFilmRate);
            dbContext.SaveChanges();

            double rateNew = dbContext.Film.Find(rateModel.FilmId).Rate;

            return rateNew;
        }

        public string FindFilmVideoBlob(string filmReference)
        {
            return blobService.DownloadBlobVideo(filmReference);
        }

        public bool AddFilm(FilmModel filmModel)
        {
            bool filmAdded = false;
            var username = filmModel.Username;
            var authorLink = dbContext.User.Find(username).ConnectionLink;

            var videoReference = СreateReferenceForBlob(username, filmModel.FilmId, filmModel.Name);
            blobService.UploadBlobVideo(videoReference, filmModel.VideoFileForm);


            var film = new Film()
            {
                AgePermition = filmModel.AgePermition,
                AuthorLink = authorLink,
                CameraMan = filmsHepler.FindFilmParticipentId(filmModel.NameCameraManArray),
                EditedBy = filmsHepler.FindFilmParticipentId(filmModel.NameEditedByArray),
                MinuteDuration = filmModel.MinuteDuration,
                MusicBy = filmsHepler.FindFilmParticipentId(filmModel.NameMusicByArray),
                Name = filmModel.Name,
                Producer = filmsHepler.FindFilmParticipentId(filmModel.NameProducerArray),
                ProductionYear = filmModel.ProductionYear,
                ScriptWriter = filmsHepler.FindFilmParticipentId(filmModel.NameScriptWriterArray),
                StageManager = filmsHepler.FindFilmParticipentId(filmModel.NameStageManagerArray),
                VideoFile = videoReference,
                //PhotoFile = photoReference,
                //Description = null,
                Date = DateTime.Now,
                Rate = 0,
                CountRated = 0
            };

            var filmCreated = dbContext.Add(film);
            dbContext.SaveChanges();

            var photoReference = Convert.ToString(film.Id);
            blobService.UploadBlobPhoto(photoReference, filmModel.PhotoFileForm);

            if (filmCreated != null)
            {
                bool categoriesCreated = true;
                foreach(var categoryId in filmModel.CategoriesArray)
                {
                    var filmCategory = new FilmCategory()
                    {
                        FilmId = filmCreated.Entity.Id,
                        CategoryId = categoryId
                    };
                    var categoryAdded = dbContext.FilmCategory.Add(filmCategory);
                    if(categoryAdded == null)
                    {
                        categoriesCreated = false;
                    }
                }

                bool actorsCreated = true;
                foreach (var actor in filmModel.ActorsArray)
                {
                    var filmActor = new FilmActor()
                    {
                        FilmId = filmCreated.Entity.Id,
                        ActorId = filmsHepler.FindFilmParticipentId(actor)
                    };
                    var actorAdded = dbContext.FilmActor.Add(filmActor);
                    if (actorAdded == null)
                    {
                        actorsCreated = false;
                    }
                }

                bool countriesCreated = true;
                foreach (var country in filmModel.CountriesArray)
                {
                    var filmCountry = new FilmCountry()
                    {
                        FilmId = filmCreated.Entity.Id,
                        CountryId = country
                    };
                    var countryAdded = dbContext.FilmCountry.Add(filmCountry);
                    if (countryAdded == null)
                    {
                        countriesCreated = false;
                    }
                }

                if(categoriesCreated && actorsCreated && countriesCreated)
                {
                    dbContext.SaveChanges();
                    var clientFilmAdded = new ClientFilmAdded()
                    {
                        FilmId = filmCreated.Entity.Id,
                        Username = username,
                        Status = "0"
                    };
                    var clientFilmCreated = dbContext.ClientFilmAdded.Add(clientFilmAdded);
                    dbContext.SaveChanges();
                    if(clientFilmCreated != null)
                    {
                        filmAdded = true;
                    }
                }
                else
                {
                    var filmToRemove = dbContext.Film.Find(filmCreated.Entity.Id);
                    dbContext.Film.Remove(filmToRemove);
                    dbContext.SaveChanges();
                }

            }

            return filmAdded;
        }

        public bool DeleteFilm(long filmId)
        {
            var isFilmDeleted = false;
            //удалять из облака
            var clientFilmAdded = dbContext.ClientFilmAdded.Where(o => o.FilmId == filmId);
            dbContext.ClientFilmAdded.RemoveRange(clientFilmAdded);

            var filmCountry = dbContext.FilmCountry.Where(o => o.FilmId == filmId);
            dbContext.FilmCountry.RemoveRange(filmCountry);

            var filmActor = dbContext.FilmActor.Where(o => o.FilmId == filmId);
            dbContext.FilmActor.RemoveRange(filmActor);

            var filmCategory = dbContext.FilmCategory.Where(o => o.FilmId == filmId);
            dbContext.FilmCategory.RemoveRange(filmCategory);

            var clientFilmRate = dbContext.ClientFilmRate.Where(o => o.FilmId == filmId);
            dbContext.ClientFilmRate.RemoveRange(clientFilmRate);

            dbContext.SaveChanges();

            var film = dbContext.Film.Find(filmId);
            dbContext.Film.Remove(film);

            dbContext.SaveChanges();

            isFilmDeleted = true;

            return isFilmDeleted;
        }

        public bool DenyFilm(long filmId)
        {
            var isFilmDenied = DeleteFilm(filmId);
            return isFilmDenied;
        }

        private IEnumerable<FilmModel> BuildModel(IEnumerable<long> filmsId)
        {
            var filmModels = new List<FilmModel>();
            foreach (var filmId in filmsId)
            {
                filmModels.Add(BuildModel(filmId));
            }
            return filmModels;
        }

        private IQueryable<Film> SearchFilmByName(string filmName, IQueryable<Film> films)
        {
            var searchName = filmName.Trim().ToLower();
            films = films.Where(o => o.Name.ToLower().Contains(searchName));
            return films;
        }

        private IQueryable<Film> SearchFilmByYearStart(int? yearStart, IQueryable<Film> films)
        {
            films = films.Where(o => o.ProductionYear >= yearStart);
            return films;
        }

        private IQueryable<Film> SearchFilmByYearEnd(int? yearEnd, IQueryable<Film> films)
        {
            films = films.Where(o => o.ProductionYear <= yearEnd);
            return films;
        }

        private IQueryable<Film> SearchFilmByCountry(long[] countries, IQueryable<Film> films)
        {
            var filmsDictionary = new Dictionary<long, long>();
            foreach (var countryId in countries)
            {
              var filmCountries = dbContext.FilmCountry.Where(o => o.CountryId == countryId);
              foreach (var filmcountry in filmCountries)
              {
                  filmsDictionary[filmcountry.FilmId] = 1;
              }
            }

            films = films.Where(o => filmsDictionary.ContainsKey(o.Id));

            return films;
        }

        private IQueryable<Film> SearchFilmByCategory(long[] categories, IQueryable<Film> films)
        {
            var filmsDictionary = new Dictionary<long, long>();
            foreach (var categoryId in categories)
            {
              var filmCategories = dbContext.FilmCategory.Where(o => o.CategoryId == categoryId);
              foreach (var filmcountry in filmCategories)
              {
                filmsDictionary[filmcountry.FilmId] = 1;
              }
            }

            films = films.Where(o => filmsDictionary.ContainsKey(o.Id));

            return films;
        }
        private IQueryable<Film> SearchFilmByStageManager(long[] managers, IQueryable<Film> films)
        {
            var filmsDictionary = new Dictionary<long, long>();
            foreach (var managerId in managers)
            {
                var filmsManagers = films.Where(o => o.StageManager == managerId);
                foreach (var film in filmsManagers)
                {
                  filmsDictionary[film.Id] = 1;
                }
            }

            films = films.Where(o => filmsDictionary.ContainsKey(o.Id));

            return films;
        }


        private IEnumerable<long> DbSearchFilm(SearchModel searchModel)
        {
            var filmsId = GetAcceptedFilms();
            var films = dbContext.Film.Where(o => filmsId.Contains(o.Id));
            if(searchModel.FilmName != "")
            {
                films = SearchFilmByName(searchModel.FilmName, films);
            }
            if(searchModel.YearStart != null)
            {
                films = SearchFilmByYearStart(searchModel.YearStart, films);
            }
            if (searchModel.YearEnd != null)
            {
                films = SearchFilmByYearEnd(searchModel.YearEnd, films);
            }
            if ((searchModel.CategoriesId != null) && (searchModel.CategoriesId.Count() != 0))
            {
                films = SearchFilmByCategory(searchModel.CategoriesId, films);
            }
            if ((searchModel.CountriesId != null) && (searchModel.CountriesId.Count() != 0))
            {
                films = SearchFilmByCountry(searchModel.CountriesId, films);
            }
            if ((searchModel.StageManagerId != null) && (searchModel.StageManagerId.Count() != 0))
            {
                films = SearchFilmByStageManager(searchModel.StageManagerId, films);
            }

            var result = new List<long>();

            foreach(var film in films)
            {
                result.Add(film.Id);
            }

            return result;
        }

        private long GetFilmStatus(long filmId)
        {
            var status = dbContext.ClientFilmAdded.Where(o => o.FilmId == filmId).First().Status;
            return Convert.ToInt64(status);
        }

        private List<long> GetAcceptedFilms()
        {
            var filmsAdded = dbContext.ClientFilmAdded.Where(o => o.Status == "1");
            var filmsAddedIds = new List<long>();
            foreach (var film in filmsAdded)
            {
                filmsAddedIds.Add(film.FilmId);
            }

            return filmsAddedIds;
        }

        private string СreateReferenceForBlob(string username, long filmId, string fileName)
        {
            string reference = username + Convert.ToString(filmId) + fileName;
            return reference;
        }
    }
}
