using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Comments
{
    public interface ICommentsService
    {
        void AddComment(CommentModel commentModel);
        void DeleteComment(CommentModel commentModel);
        IEnumerable<CommentModel> GetComments(CommentRequestModel requestModel);
        IEnumerable<CommentModel> GetParentComment(CommentRequestModel requestModel);
    }
}
