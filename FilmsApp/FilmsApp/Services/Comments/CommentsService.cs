using FilmsApp.Data;
using FilmsApp.Data.DatabaseModels;
using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Comments
{
    public class CommentsService : ICommentsService
    {
        private int commentsPerPage;
        private FilmsDbContext dbContext;

        public CommentsService(FilmsDbContext dbContext)
        {
            this.dbContext = dbContext;
            commentsPerPage = 10;
        }

        public void AddComment(CommentModel commentModel)
        {
            var newComment = new ClientFilmComment
            {
                Date = DateTime.Now,
                Comment = commentModel.CommentText,
                FilmId = commentModel.FilmId,
                IsDeleted = false,
                ParentCommentId = commentModel.ParentCommentId,
                Username = commentModel.Username
            };
            dbContext.ClientFilmComment.Add(newComment);
            dbContext.SaveChanges();
        }

        public void DeleteComment(CommentModel commentModel)
        {
            var delete = dbContext.ClientFilmComment.Where(c => c.CommentId == commentModel.CommentId).ToList()[0];
            dbContext.ClientFilmComment.Remove(delete);
            dbContext.SaveChanges();
        }

        public IEnumerable<CommentModel> GetComments(CommentRequestModel requestModel)
        {
            ValidateRequest(ref requestModel);
            
            return GetCommentsOnSpecificPage(requestModel, GetAndSortExistCommentsForFilm(requestModel));
        }

        public IEnumerable<CommentModel> GetParentComment(CommentRequestModel requestModel)
        {
            ValidateRequest(ref requestModel);

            var allComments = GetAndSortExistCommentsForFilm(requestModel);

            requestModel.Page = FindParentPage(requestModel.ParentCommentId ?? -1, allComments);

            return GetCommentsOnSpecificPage(requestModel, allComments); ;
        }

        private IQueryable<ClientFilmComment> GetAndSortExistCommentsForFilm(CommentRequestModel requestModel)
        {
            var comments = dbContext.ClientFilmComment.Where(c => (c.FilmId == requestModel.FilmId) && !c.IsDeleted);

            if (requestModel.IsDateASC ?? true)
                comments = comments.OrderBy(c => c.Date);
            else
                comments = comments.OrderByDescending(c => c.Date);

            return comments;
        }

        private List<CommentModel> GetCommentsOnSpecificPage(CommentRequestModel requestModel, IQueryable<ClientFilmComment> source)
        {
            var comments = source.Skip(commentsPerPage*requestModel.Page ?? 0).Take(commentsPerPage);
            
            var result = new List<CommentModel>();
            foreach (var comment in comments)
            {
                var resultComment = new CommentModel
                {
                    CommentId = comment.CommentId,
                    CommentText = comment.Comment,
                    Date = comment.Date.ToString(),
                    FilmId = comment.FilmId,
                    ParentCommentId = comment.ParentCommentId,
                    Username = comment.Username
                };
                result.Add(resultComment);
            }
            return result;
        }

        private int FindParentPage(long parentId, IQueryable<ClientFilmComment> source)
        {
            if (parentId == -1)
                throw new Exception("Недопустимый Id родительского комментария.");

            int position = 0;
            foreach (var item in source)
            {
                if (item.CommentId == parentId)
                    break;
                position++;
            }
            if (position == source.Count())
                position = 0;

            return position/commentsPerPage;
        }

        private void ValidateRequest(ref CommentRequestModel requestModel)
        {
            if (requestModel.Page != null && requestModel.Page < 0)
                    requestModel.Page = 0;
        }
    }
}
