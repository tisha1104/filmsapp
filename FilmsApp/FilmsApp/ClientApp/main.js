import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import Message from 'vue-m-message';
import App from './App.vue';
import StartPage from './components/StartPage.vue';
import SignUp from './components/SignUp.vue';
import Login from './components/Login.vue';
import SearchFilms from './components/SearchFilms.vue';
import FilmPage from './components/FilmPage.vue';
import AddFilm from './components/AddFilm.vue';
import Store from './Store';
import FilmsTop from './components/FilmsTop.vue';
import FilmsUser from './components/FilmsUser.vue';
import FilmsNew from './components/FilmsNew.vue';
import AcceptFilmPage from './components/AcceptFilmPage.vue';
import ManageFilmPage from './components/ManageFilmPage.vue';


Vue.use(Router);
Vue.use(Meta);
Vue.use(Message);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'start',
            component: StartPage,
            props: true
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/signup',
            name: 'signup',
            component: SignUp
        },
        {
            path: '/search',
            name: 'search',
            component: SearchFilms,
            props: true
        },
        {
            path: '/film',
            name: 'filmPage',
            component: FilmPage,
            props: true
        },
        {
            path: '/add',
            name: 'addFilm',
            component: AddFilm,
            props: true
        },
        {
            path: '/my',
            name: 'filmsUser',
            component: FilmsUser,
            props: true
        },
        {
            path: '/top',
            name: 'filmsTop',
            component: FilmsTop,
            props: true
        },
        {
            path: '/new',
            name: 'filmsNew',
            component: FilmsNew,
            props: true
        },
        {
            path: '/suggestions',
            name: 'acceptFilmPage',
            component: AcceptFilmPage,
            props: true
        },
        {
            path: '/manage',
            name: 'manageFilmPage',
            component: ManageFilmPage,
            props: true
        }
    ],
    mode: 'history'
});

new Vue({
    el: '#app',
    data: Store,
    render: h => h(App),
    router
});
