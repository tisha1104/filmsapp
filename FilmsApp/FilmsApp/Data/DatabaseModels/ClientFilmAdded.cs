﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class ClientFilmAdded
    {
        public long FilmId { get; set; }
        public string Username { get; set; }
        public string Status { get; set; }

        public Film Film { get; set; }
        public User UsernameNavigation { get; set; }
    }
}
