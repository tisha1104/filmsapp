﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class Country
    {
        public Country()
        {
            FilmCountry = new HashSet<FilmCountry>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<FilmCountry> FilmCountry { get; set; }
    }
}
