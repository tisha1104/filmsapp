﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class FilmCountry
    {
        public long FilmId { get; set; }
        public long CountryId { get; set; }

        public Country Country { get; set; }
        public Film Film { get; set; }
    }
}
