using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class ClientFilmComment
    {
        public long CommentId { get; set; }
        public long FilmId { get; set; }
        public string Username { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public long? ParentCommentId { get; set; }
        public bool IsDeleted { get; set; }
        public Film Film { get; set; }
        public User UsernameNavigation { get; set; }
    }
}
