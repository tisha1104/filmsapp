using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class FilmCategory
    {
        public long FilmId { get; set; }
        public long CategoryId { get; set; }
        public Category Category { get; set; }
        public Film Film { get; set; }
    }
}
