﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class FilmActor
    {
        public long FilmId { get; set; }
        public long ActorId { get; set; }

        public FilmParticipant Actor { get; set; }
        public Film Film { get; set; }
    }
}
