﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class User
    {
        public User()
        {
            ClientFilmAdded = new HashSet<ClientFilmAdded>();
            ClientFilmComment = new HashSet<ClientFilmComment>();
            ClientFilmDelayed = new HashSet<ClientFilmDelayed>();
            ClientFilmFavourite = new HashSet<ClientFilmFavourite>();
            ClientFilmRate = new HashSet<ClientFilmRate>();
            ClientFilmWatched = new HashSet<ClientFilmWatched>();
        }

        public string Username { get; set; }
        public DateTime BirthDate { get; set; }
        public string ConnectionLink { get; set; }
        public string FatherName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mail { get; set; }
        public string ProfileImage { get; set; }
        public string Role { get; set; }
        public string Sex { get; set; }

        public UserPassword UserPassword { get; set; }
        public ICollection<ClientFilmAdded> ClientFilmAdded { get; set; }
        public ICollection<ClientFilmComment> ClientFilmComment { get; set; }
        public ICollection<ClientFilmDelayed> ClientFilmDelayed { get; set; }
        public ICollection<ClientFilmFavourite> ClientFilmFavourite { get; set; }
        public ICollection<ClientFilmRate> ClientFilmRate { get; set; }
        public ICollection<ClientFilmWatched> ClientFilmWatched { get; set; }
    }
}
