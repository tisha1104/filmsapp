﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FilmsApp.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "parentCommentId",
                table: "ClientFilmComment",
                nullable: true,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "parentCommentId",
                table: "ClientFilmComment",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
