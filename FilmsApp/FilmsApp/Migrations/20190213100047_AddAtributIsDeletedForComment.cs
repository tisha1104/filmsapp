﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FilmsApp.Migrations
{
    public partial class AddAtributIsDeletedForComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ClientFilmComment",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ClientFilmComment");
        }
    }
}
