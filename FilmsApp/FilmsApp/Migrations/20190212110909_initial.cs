using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FilmsApp.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientFilmComment");

            migrationBuilder.CreateTable(
                name: "ClientFilmComment",
                columns: table => new
                {
                    commentId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    filmId = table.Column<long>(nullable: false),
                    username = table.Column<string>(maxLength: 50, nullable: true),
                    comment = table.Column<string>(maxLength: 1000, nullable: false),
                    date = table.Column<DateTime>(type: "date", nullable: false),
                    parentCommentId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientFilmComment", x => x.commentId);
                    table.ForeignKey(
                        name: "FK_ClientFilmComment_Film",
                        column: x => x.filmId,
                        principalTable: "Film",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientFilmComment_User",
                        column: x => x.username,
                        principalTable: "User",
                        principalColumn: "username",
                        onDelete: ReferentialAction.Restrict);
                });

            
            migrationBuilder.CreateIndex(
                name: "IX_ClientFilmComment_filmId",
                table: "ClientFilmComment",
                column: "filmId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientFilmComment_username",
                table: "ClientFilmComment",
                column: "username");

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropTable(
                name: "ClientFilmComment");

            
        }
    }
}
